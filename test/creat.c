
#include "stdio.h"
#include "stdlib.h"

int
do_creat (char *fname) {
    int fd;

    printf ("creating %s...\n", fname);
    fd = creat (fname);
    if (fd >= 0) {
	printf ("...passed (fd = %d)\n", fd);
    } else {
	printf ("...failed (%d)\n", fd);
	exit (-1);
    }
    return fd;
}

int
main ()
{
    do_creat("file1");
    do_creat("file2");
    do_creat("file3");
    do_creat("file4");
    do_creat("file5");
    do_creat("file6");
    do_creat("file7");
    do_creat("file8");
    do_creat("file9");
    do_creat("file10");
    do_creat("file11");
    do_creat("file12");
    do_creat("file13");
    do_creat("file14");
    do_creat("file15");
    do_creat("file16");
}
