package nachos.vm;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import nachos.vm.*;

/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
		super();
	}

	/**
	 * Initialize this kernel.
	 */
	public void initialize(String[] args) {

		super.initialize(args);
		VMProcess.initializeStatic();
		swapFile = ThreadedKernel.fileSystem.open(swapFileName,true);
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		super.run();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		//DELETE SWAP FILE AND CLOSE FILESYSTEM
		swapFile.close();

		ThreadedKernel.fileSystem.remove(swapFileName);
		super.terminate();
	}

	// dummy variables to make javac smarter
	private static VMProcess dummy1 = null;

	private static final char dbgVM = 'v';


	public static OpenFile swapFile;

	private static final String swapFileName = "nachosSwapFile";
}

//CREATE FILESYSTEM VAR and FILE
