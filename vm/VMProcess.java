package nachos.vm;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;
import nachos.vm.*;

import java.util.*;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {

		super.saveState();
		int tlb_size = Machine.processor().getTLBSize();
		for(int i = 0; i < tlb_size; i++){
			TranslationEntry toSync = Machine.processor().readTLBEntry(i);
			if(toSync.valid == true){
				pageTable[toSync.vpn].dirty = toSync.dirty;
				pageTable[toSync.vpn].used = toSync.used;
				IPT_TE[toSync.ppn].dirty = toSync.dirty;
				IPT_TE[toSync.ppn].used = toSync.used;
			}
			toSync.valid = false;
			Machine.processor().writeTLBEntry(i,toSync);
		}
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 

	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		boolean ret = dontLoadSections();
		ArrayList<Integer> swapTable = new ArrayList<Integer>(numPages);
		swapTable.ensureCapacity(numPages);
		for(int i = 0; i < numPages; i++){
			swapTable.add(i, -1);
			pageTable[i] = new TranslationEntry();
			pageTable[i].vpn = i;
		}
		swapMap.put(processID, swapTable);
		return ret;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {

		iptLock.acquire();
		for(int i = 0; i < Machine.processor().getNumPhysPages(); i++){
			if(IPT_process[i] == super.processID){
				UserKernel.freePages.add(i);
				IPT_TE[i].used = false;
				IPT_process[i] = -1;
				
			}
		}
		iptLock.release();


	}


	//handlePin Function
	@Override
	protected int pinVirtualPage(int vpn, boolean isUserWrite){
		if (vpn < 0 || vpn >= pageTable.length)
	    		return -1;
		

		if (!(pageTable[vpn].valid)){
	    		handlePageFault(vpn);
		}

		if (isUserWrite) {
	    		if (pageTable[vpn].readOnly)
				return -1;
	    		pageTable[vpn].dirty = true;
		}

		pageTable[vpn].used = true;
		iptLock.acquire();

		IPT_pincount[pageTable[vpn].ppn] = 1;
		pinLock.acquire();
		pinTracker--;
		pinLock.release();
		
		IPT_TE[pageTable[vpn].ppn] = pageTable[vpn];
		iptLock.release();
		
		return pageTable[vpn].ppn;

	}

	@Override
	protected void unpinVirtualPage(int vpn){
		if(vpn < 0 || vpn >= pageTable.length){
			return;
		}
		iptLock.acquire();
		if(IPT_pincount[pageTable[vpn].ppn] == 1){
			IPT_pincount[pageTable[vpn].ppn] = 0;
			pinLock.acquire();
			pinTracker++;
			pinCondition.wakeAll();
			pinLock.release();
		}
		iptLock.release();

	}

private static int tlb_y = 0;

	private void handleTLBMiss(){
		int vaddr_x = Machine.processor().readRegister(Processor.regBadVAddr);
		int vpn_x = Processor.pageFromAddress(vaddr_x);

	
		//Search TLB to find replacement option
		int tlb_size = Machine.processor().getTLBSize();
		int y = tlb_y;
		TranslationEntry toCheck;
		boolean tlbFound = false;
		for(int i = tlb_y; i< (tlb_y+tlb_size); i++){
			toCheck = Machine.processor().readTLBEntry(i%tlb_size);
			if(toCheck.valid == false){
				y = i%tlb_size;
				tlbFound = true;
				break;
			}
		}
		//if no invalid TLB options choose one at random
		if(tlbFound == false){
			y = Lib.random(tlb_size);
			//System.out.println("y:"+y+"tlbs:"+tlb_size);

			//y = y%tlb_size;
		}
		toCheck = Machine.processor().readTLBEntry(y);
		
		//System.out.println("y:" +y);

		iptLock.acquire();

		//sync TLB victim with pageTable if entry is valid
		if(toCheck.valid == true){
			pageTable[toCheck.vpn].valid = true;
			pageTable[toCheck.vpn].dirty = toCheck.dirty;
			pageTable[toCheck.vpn].used = toCheck.used;
			IPT_TE[toCheck.ppn] = pageTable[toCheck.vpn];
		}

		//make sure that the ppn is owned by current process
		boolean ppn_owned = (IPT_process[pageTable[vpn_x].ppn] == processID);
		//make sure that the current ppn is assigned to current vpn_x
		boolean IPT_vpn_match = (IPT_TE[pageTable[vpn_x].ppn].vpn == vpn_x);
		
		//check if the vpn is loaded in memory
		if(pageTable[vpn_x].valid == false){
			handlePageFault(vpn_x);
		}
		else if(!(ppn_owned)||!(IPT_vpn_match)){
		//	System.out.println("CHECK");
			handlePageFault(vpn_x);
		}
		iptLock.release();
		//set page's used to true because it is being accessed
		pageTable[vpn_x].used = true;
		pageTable[vpn_x].valid = true;

		Machine.processor().writeTLBEntry(y, pageTable[vpn_x]);
		tlb_y = y+1;
		
		
	}




	private void printPTE(TranslationEntry pte ){
		System.out.print("vpn: " + pte.vpn);
		System.out.print("\tppn: " + pte.ppn);
		System.out.print("\tvalid: " + pte.valid);
		System.out.print("\treadOnly: " + pte.readOnly);
		System.out.print("\tused: " + pte.used);
		System.out.print("\tdirty: " + pte.dirty + "\n");
	}	




	private void handlePageFault(int vpn_toLoad){
		//Machine.stats.numPageFaults++;
		int victim_ppn = -1;
		
		UserKernel.memoryLock.acquire();
		if(UserKernel.freePages.size() > 0){
			victim_ppn = ((Integer)UserKernel.freePages.removeFirst()).intValue();
		}
		UserKernel.memoryLock.release();

		//if no free pages, user clock alg. find a free page 
		if(victim_ppn == -1){

			clockLock.acquire();
			while(IPT_TE[clock_victim].used == true){
				
				pinLock.acquire();
				if(pinTracker == 0){
					pinCondition.sleep();
				}
				pinLock.release();
				if(IPT_pincount[clock_victim] == 0){
					IPT_TE[clock_victim].used = false;
				}
				clock_victim = (clock_victim+1)%NUM_PHYS_PAGES;
			}
			victim_ppn = clock_victim;
			clock_victim = (clock_victim+1)%NUM_PHYS_PAGES;
			clockLock.release();

		}


					//if victim vpn is a valid entry in the TLB, invalidate it
		check_invalidate_tlb(IPT_TE[victim_ppn].vpn, victim_ppn);
					//System.out.println("cit: "+ cit);
					//
		//if victim physical page is dirty, write it to swap file
		if(IPT_TE[victim_ppn].dirty == true){
			handleSwapOut(victim_ppn);
		}

				
		if(IPT_process[victim_ppn] == processID){
		//	System.out.println("INVALIDATE VPN: "+ IPT_TE[victim_ppn].vpn);
			pageTable[IPT_TE[victim_ppn].vpn].valid = false;
		}
		
		//load the vpn wanted into the victim ppn
		handleLoad(victim_ppn, vpn_toLoad);
		
		pageTable[vpn_toLoad].ppn = victim_ppn;
		pageTable[vpn_toLoad].valid = true;
		pageTable[vpn_toLoad].vpn = vpn_toLoad;

		
		IPT_process[victim_ppn] = super.processID;
		IPT_TE[victim_ppn] = pageTable[vpn_toLoad];
	}



	
	private void check_invalidate_tlb(int vpn, int ppn){
		int tlb_size = Machine.processor().getTLBSize();
		int y = -1;
		TranslationEntry toCheck = new TranslationEntry();
		for(int i = 0; i< tlb_size; i++){
			toCheck = Machine.processor().readTLBEntry(i);
			if(toCheck.valid == true){
				{
					if(toCheck.ppn == ppn){
						y = i;
						break;
					}
				}
			}
		}
		if(y != -1){
			//System.out.println("PRINTSTAATEMETNT");
			toCheck.valid = false;
			pageTable[vpn].dirty = toCheck.dirty;
			pageTable[vpn].used = toCheck.used;
			Machine.processor().writeTLBEntry(y, toCheck);
		}
	}






	private void handleSwapOut(int victim_ppn){
		//Machine.stats.numDiskWrites++;
		int swapF_ind = -1;
		int swap_vpn = IPT_TE[victim_ppn].vpn;


		ArrayList<Integer> swapTable = swapMap.remove(IPT_process[victim_ppn]);
		
		swapF_ind = swapTable.get(swap_vpn);
		if(swapF_ind == -1){
			if(freeSwapPages.size() == 0){
				incrementSwapLL();
			}
			swapF_ind = freeSwapPages.remove(0);
			swapTable.set(swap_vpn, swapF_ind);
		}
		writeSwap(swapF_ind, victim_ppn);
		swapMap.put(IPT_process[victim_ppn], swapTable);

	}

	private void handleLoad(int victim_ppn, int vpn_toLoad){
		//Machine.stats.numDiskReads++;
		ArrayList<Integer> swapTable = swapMap.get(super.processID);

		if(swapTable.get(vpn_toLoad) == -1){
			handleLoad_coff(victim_ppn, vpn_toLoad);
		}
		else{
			int swapF_ind = swapTable.get(vpn_toLoad);
			handleLoad_swap(victim_ppn, swapF_ind);
			pageTable[vpn_toLoad].readOnly = false;
			pageTable[vpn_toLoad].dirty = false;
		}
		
		if(IPT_process[victim_ppn] == processID){
			pageTable[IPT_TE[victim_ppn].vpn].valid = false;
		
		}
		
	}




int printcnt = 0;
	private void handleLoad_coff(int victim_ppn, int vpn_toLoad){
		int vpn = 0;
//if(printcnt < 20){
//printcnt++;
//System.out.println("loadvpn:" +vpn_toLoad +"\tvicppn:"+victim_ppn);
//}

		for(int s = 0; s < coff.getNumSections(); s++){
			CoffSection section = coff.getSection(s);
			int startVPN = section.getFirstVPN();
			int endVPN =startVPN + section.getLength();
//System.out.println("start: " + startVPN + "end: "+endVPN);
			if(endVPN <= vpn_toLoad){
				vpn += section.getLength();
				continue;
			}
			else{
				int section_ind = vpn_toLoad - startVPN;
				vpn += section_ind;
				if(vpn == vpn_toLoad){
					pageTable[vpn_toLoad].readOnly = section.isReadOnly();
					pageTable[vpn_toLoad].dirty = false;
//if(printcnt <20){
//printcnt++;
//System.out.println("COFF");
//}
//System.out.println("length: "+ section.getLength()+ "sind: " +section_ind+ "\tvicppn:"+victim_ppn+"\n");

					section.loadPage(section_ind, victim_ppn);
					return;
				}
			}
		}
		//If not in Coff, load a stack page
		if(vpn_toLoad < vpn+9){
//System.out.println("STACK");
			pageTable[vpn_toLoad].dirty = true;
			pageTable[vpn_toLoad].readOnly = false;
			byte[] memory = Machine.processor().getMemory();
			int pagesize = Processor.pageSize;
			Arrays.fill(memory, victim_ppn*pagesize, (victim_ppn+1)*pagesize, (byte)0);
		}
		
		

	}

	private void handleLoad_swap(int victim_ppn, int swapF_ind){
		readSwap(swapF_ind, victim_ppn);
	}


	private void writeSwap(int swapWrite_ind, int ppn_to_write){
		//get physical memory
		byte[] memory = Machine.processor().getMemory();
		int pagesize = Processor.pageSize;
		VMKernel.swapFile.write(swapWrite_ind*pagesize, memory, ppn_to_write*pagesize,pagesize);
	}

	private void readSwap(int swapRead_ind, int ppn_to_write){
		//get physical memory
		byte[] memory = Machine.processor().getMemory();
		int pagesize = Processor.pageSize;
		VMKernel.swapFile.read(swapRead_ind*pagesize, memory, ppn_to_write*pagesize,pagesize);
	}


	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			handleTLBMiss();
			break;
		default:
			super.handleException(cause);
			break;
		}
	}



	public static void initializeStatic(){
		int x = Machine.processor().getNumPhysPages();
		pinTracker = x;
		iptLock = new Lock();
		clockLock = new Lock();
		pinLock = new Lock();
		pinCondition = new Condition(pinLock);
		incrementSwapLL();

		iptLock.acquire();
		for(int i=0; i< x; i++){
			IPT_TE[i] = new TranslationEntry();
			IPT_TE[i].dirty = false;
			IPT_process[i] = -1;
			IPT_pincount[i] = 0;
		}
		iptLock.release();


	}

	//Increment LL to hold more index options	
	private static void incrementSwapLL(){

		for(int i = swapLLsize; i < swapLLsize + 32; i++){
			freeSwapPages.add(i);
		}
		swapLLsize += 32;
	}

	private static final int pageSize = Processor.pageSize;

	private static final char dbgProcess = 'a';

	private static final char dbgVM = 'v';


	private static int clock_victim = 0;
	private static Lock clockLock;

	private static final int NUM_PHYS_PAGES= Machine.processor().getNumPhysPages();
	

	//private static List<Integer> freePageList = new LinkedList<Integer>();

	private static Map<Integer,ArrayList<Integer>> swapMap = new HashMap<Integer, ArrayList<Integer>>(500);
	
	//3 Part IPT
	private static TranslationEntry[] IPT_TE = new TranslationEntry[Machine.processor().getNumPhysPages()];
	private static int[] IPT_process = new int[Machine.processor().getNumPhysPages()];
	private static int[] IPT_pincount = new int[Machine.processor().getNumPhysPages()];
	private static Lock iptLock;

	private static int pinTracker;
	private static Lock pinLock;
	private static Condition pinCondition;

	private static int swapLLsize = 0;

	

	private static List<Integer> freeSwapPages = new LinkedList<Integer>();
}
//static
// LL for freeswappages
// array for IPT
//
// nonstatic
// array of TranslationEntry for PT
// ^samesize array for swapfile tracking
